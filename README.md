# README.md #

### What is this repository for? ###
We will use this repo (short for repository) as a way to practice cloning, making changes to your code, tracking, and committing your changes in git.

### Here are the commands we will be exposed to ###


```
#!python

git clone addres-to-remote-repo
git init //initialize git repo
git remote add origin https://github.com/username/repo
git remote -v //check your remote
git add filename
git status
git commit -m "comment"
git log



git remote show origin
git push origin master // to push to master from local repo
git pull origin master // to pull from master from prod repo

git checkout filename //to revert to a file previous version

git diff origin/pre-master...pre-master //to see diff when pre-master is ahead
git diff filename

git branch
git branch -a //display all branches, including remote branches
git checkout -b branchName
git checkout branchName
git merge master branchName

git rm --cached directory/file //after adding something to .gitignore
```

### These commands will be the ones we use the most ###


```
#!python

git init //initialize git repo
git add filename //add files to be commited
git status //check the status of our repository
git commit -m "comment"  //commit the added files that contain code changes and add a comment to your commit at the same time
git push origin master // to push to master from local repo
```



### Who do I talk to? ###
If you have any question talk to me, or checkout these tutorials:

```
#!python

https://www.atlassian.com/git/tutorials
https://git-scm.com/book/en/v2
http://rogerdudler.github.io/git-guide/
```
